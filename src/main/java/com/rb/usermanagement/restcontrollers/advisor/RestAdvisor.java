package com.rb.usermanagement.restcontrollers.advisor;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Optional;

@ControllerAdvice
public class RestAdvisor {

    private ResponseEntity<ErrorResponse> error(Exception exception, HttpStatus httpStatus, String logRef) {
        final String message = Optional.of(exception.getMessage()).orElse(exception.getClass().getSimpleName());
        return new ResponseEntity< >(ErrorResponse.builder().error(message).localMessage(logRef).build(), httpStatus);
    }
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> assertionException(Exception e) {
        return error(e, HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
    }
}
