package com.rb.usermanagement.restcontrollers.advisor;

import lombok.*;

@Data
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {
    private String error;
    private String localMessage;
}
