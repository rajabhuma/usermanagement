package com.rb.usermanagement.restcontrollers;

import com.rb.usermanagement.entities.Role;
import com.rb.usermanagement.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/roles")
public class RolesRestController {

    @Autowired
    private RoleRepository roleRepo;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<String> getAllRoles() {
        return roleRepo.findAll().stream().map(Role::getRoleName).collect(Collectors.toList());
    }

}
