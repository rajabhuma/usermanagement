package com.rb.usermanagement.restcontrollers;

import com.rb.usermanagement.entities.Company;
import com.rb.usermanagement.entities.Users;
import com.rb.usermanagement.repositories.CompanyRepository;
import com.rb.usermanagement.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/company")
public class CompanyRestController {

    @Autowired
    private CompanyRepository companyRepo;

    @Autowired
    private UserRepository userRepo;

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public List<Company> getAllCompanys() {
        return companyRepo.findAll();
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Company create(@RequestBody Company company) throws Exception {

        List<Company> filtered = companyRepo.findAllByCompanyNameContainingIgnoreCaseAndCityContainingIgnoreCase(company.getCompanyName(), company.getCity());
        filtered = filtered.stream()
                .filter(obj -> obj.getCompanyName().equalsIgnoreCase(company.getCompanyName())
                        && obj.getCity().equalsIgnoreCase(company.getCity()))
                .collect(Collectors.toList());
        if (filtered.isEmpty()) {
            return companyRepo.save(company);
        }

        throw new Exception("Same company details found for company id: " + filtered.get(0).getCompanyId());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String delete(@PathVariable("id") Long id, Authentication auth) throws Exception {

        Optional<Company> company = companyRepo.findById(id);

        if (!company.isPresent()) { throw new Exception("Company not found"); }

        List<Users> thisCompanyUsers = userRepo.findAllByCompany(company.get());

        if (!thisCompanyUsers.isEmpty()) { throw new Exception("Few users are already under this company. Please remove users and then try again."); }

        companyRepo.deleteById(id);

        if (companyRepo.findById(id).isPresent()) { throw new Exception("Unable to delete entry"); }

        return "Company successfully deleted";
    }
}
