package com.rb.usermanagement.restcontrollers;

import com.rb.usermanagement.entities.Company;
import com.rb.usermanagement.entities.Role;
import com.rb.usermanagement.entities.Users;
import com.rb.usermanagement.repositories.CompanyRepository;
import com.rb.usermanagement.repositories.RoleRepository;
import com.rb.usermanagement.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserRestController {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private CompanyRepository companyRepo;

    @Autowired
    private RoleRepository roleRepo;

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Users createUser(@RequestBody Map<String, Object> user) throws Exception {
        Object firstName = user.getOrDefault("firstName", null);
        Object lastName = user.getOrDefault("lastName", null);
        Object userName = user.getOrDefault("userName", null);
        Object age = user.getOrDefault("age", null);
        Object company = user.getOrDefault("company", null);
        Object role = user.getOrDefault("role", null);

        if (firstName == null || !(firstName instanceof String) || firstName.toString().equalsIgnoreCase("")) {
            throw new Exception("First name is invalid");
        }
        if (lastName == null || !(lastName instanceof String) || lastName.toString().equalsIgnoreCase("")) {
            throw new Exception("Last name is invalid");
        }
        if (userName == null || !(userName instanceof String) || userRepo.findByUsername(userName.toString().toLowerCase()).isPresent()) {
            throw new Exception("Invalid user name or Username already taken");
        }

        if (company == null || !(company instanceof Integer)) {
            throw new Exception("Invalid company");
        }
        Optional<Company> companyObj = companyRepo.findById(Long.parseLong(company.toString()));
        if (!companyObj.isPresent()) {
            throw new Exception("Invalid company");
        }

        if (role == null || !(role instanceof String)) {
            throw new Exception("Invalid Role");
        }
        Optional<Role> roleObj = roleRepo.findOptionalByRoleName(role.toString());
        if (!roleObj.isPresent()) {
            throw new Exception("Invalid Role");
        }

        String userNameStr = userName.toString().toLowerCase();

        Integer ageInt = Optional.ofNullable(age.toString().equalsIgnoreCase("") ? null : Integer.parseInt(age.toString())).orElse(null);

        Users userData = Users.builder()
                .username(userNameStr)
                .password("Welcome@123")
                .firstName(firstName.toString())
                .lastName(lastName.toString())
                .age(ageInt)
                .company(companyObj.get())
                .roles(Arrays.asList(roleObj.get()))
                .build();
        return userRepo.save(userData);
    }

    @GetMapping
    public List<Users> getAll(Authentication auth) {
        Optional<Users> user = userRepo.findByUsername(auth.getName());
        List<String> roles = auth.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        if (roles.contains("ROLE_ADMIN")) {
            return userRepo.findAll();
        }
        return userRepo.findAllByCompany(user.get().getCompany());
    }

    @GetMapping("/{username}/isValid")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public Boolean isValidUser(@PathVariable("username") String username) {
        return userRepo.findByUsername(username).isPresent();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public String delete(@PathVariable("id") Long id, Authentication auth) throws Exception {
        Optional<Users> user = userRepo.findById(id);
        Optional<Users> authUser = userRepo.findByUsername(auth.getName());

        if (!user.isPresent()) { throw new Exception("User not available to delete"); }
        if (!authUser.isPresent()) { throw new Exception("Unknown Error"); }
        if (user.get().getUserId() == authUser.get().getUserId()) { throw new Exception("You can delete ur own profile"); }

        userRepo.deleteById(id);

        if (userRepo.findById(id).isPresent()) { throw new Exception("Unable to delete user"); }

        return "User Successfully deleted";
    }
}
