package com.rb.usermanagement.entities;

import com.rb.usermanagement.entities.generic.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class Users extends Auditable<String> {

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userId;

    @Column(name = "username", unique = true, nullable = false)
    private String username;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "age")
    private Integer age;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    List<Role> roles;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinTable(name = "user_company",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "company_id"))
    Company company;

    @Builder.Default
    @Column(name = "disabled")
    private Boolean disabled = false;

    @Builder.Default
    @Column(name = "account_expired")
    private Boolean accountExpired = false;

    @Builder.Default
    @Column(name = "account_locked")
    private Boolean accountLocked = false;

    @Builder.Default
    @Column(name = "credentials_expired")
    private Boolean credentialsExpired = false;

    @Builder.Default
    @Column(name = "is_default_password")
    private Boolean isDefaultPassword = true;
}
