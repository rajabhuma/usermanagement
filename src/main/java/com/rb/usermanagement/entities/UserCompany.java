package com.rb.usermanagement.entities;

import com.rb.usermanagement.entities.generic.Auditable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_company")
public class UserCompany extends Auditable<String> {

    @Id
    @Column(name = "user_company_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long userCompanyId;

    @Column(name = "user_id", nullable = false)
    private Integer userId;

    @Column(name = "company_id", nullable = false)
    private Integer companyId;
}
