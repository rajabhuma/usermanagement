package com.rb.usermanagement.repositories;

import com.rb.usermanagement.entities.Company;
import com.rb.usermanagement.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {
    Optional<Users> findByUsername(String userName);
    List<Users> findAllByCompany(Company company);
}
