package com.rb.usermanagement.repositories;

import com.rb.usermanagement.entities.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {
    List<Company> findAllByCompanyNameContainingIgnoreCaseAndCityContainingIgnoreCase(String companyName, String City);
}
