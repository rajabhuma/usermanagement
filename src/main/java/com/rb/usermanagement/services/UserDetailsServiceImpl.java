package com.rb.usermanagement.services;

import com.rb.usermanagement.entities.Role;
import com.rb.usermanagement.entities.Users;
import com.rb.usermanagement.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service(value = "userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Optional<Users> user = userRepository.findByUsername(s);

        if (user.isPresent()) {

            List<String> roles = user.get().getRoles().stream().map(Role::getRoleName).collect(Collectors.toList());

            return User.builder()
                    .username(user.get().getUsername())
                    .password(bCryptPasswordEncoder.encode(user.get().getPassword()))
                    .disabled(Optional.ofNullable(user.get().getDisabled()).orElse(false))
                    .accountExpired(Optional.ofNullable(user.get().getAccountExpired()).orElse(false))
                    .accountLocked(Optional.ofNullable(user.get().getAccountLocked()).orElse(false))
                    .credentialsExpired(Optional.ofNullable(user.get().getCredentialsExpired()).orElse(false))
                    .roles(roles.toArray(new String[0]))
                    .build();
        }

        throw new UsernameNotFoundException("User Name is not Found");
    }
}
