package com.rb.usermanagement.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {

    @GetMapping("/login")
    public String login(Authentication auth) {
        if (auth != null && auth.isAuthenticated())
            return "/";
        return "login";
    }

    @GetMapping("/")
    public ModelAndView home() {
        return new ModelAndView("redirect:/home");
    }

    @GetMapping("/home")
    public String homeMain(Authentication auth, ModelMap model) {
        return "home";
    }
}
