// User List
Vue.component('user-list-view', {
    template: '#user-list-template',
    data() {
        return {
            userFields: [
                {
                    key: 'name',
                    label: 'User Name',
                    formatter: (value, key, item) => {
                        return item.firstName + ' ' + item.lastName
                    }
                },
                {
                    key: 'company',
                    label: 'Company Name',
                    formatter: (value, key, item) => {
                        return item.company.companyName
                    }
                },
                {key: 'username', label: 'Username'},
                {key: 'age', label: 'Age'},
                {key: 'action', label: 'Actions'}
            ],
            isBusy: true,
            users: [
                {
                    firstName: '',
                    lastName: '',
                    age: 0,
                    username: '',
                    company: {
                        companyName: ''
                    }
                },
            ],
            currentPage: 1,
            perPage: 5,
            pageOptions: [5, 10, 25, 50],
            createUser: {
                firstName: '',
                lastName: '',
                userName: '',
                age: null,
                company: null,
                role: ''
            },
            companyList: [],
            rolesList: [],
            companySelected: null,
            companySelectedName: '',
            isValidUsername: false,
            userDetails: {
                firstName: '',
                lastName: '',
                username: '',
                roles: [{roleName:''}],
                company: {
                    companyId: '',
                    companyName: '',
                    address: '',
                    city: '',
                    pincode: ''
                }
            },
        }
    },
    mounted() {
        this.fetchUsers()
    },
    methods: {
        fetchUsers() {
            this.isBusy = true
            axios.get("/user")
                .then(res => {
                    this.users = res.data
                })
                .catch(() => {
                    this.users = []
                })
                .finally(() => {
                    this.isBusy = false
                })
        },
        viewClicked(row) {
            this.userDetails = row.item
            this.$bvModal.show('user-details-model')
        },
        resetUserDetails() {
            this.userDetails = {
                firstName: '',
                lastName: '',
                username: '',
                age: null,
                roles: [{roleName:''}],
                company: {
                    companyId: '',
                    companyName: '',
                    address: '',
                    city: '',
                    pincode: ''
                }
            }
        },
        deleteClicked(row) {
            axios.delete("/user/" + row.item.userId)
                .then(res => {
                    this.fetchUsers()
                    this.successModel("Confirmation", res.data)
                })
                .catch(e => {
                    this.failureModel("Failure", e.response.data.error)
                })
        },
        companyStateChanged: function (newCompany) {
            if (this.companySelected == null || newCompany != this.companySelected.name) {
                let filtered = this.companyList.filter(item => {
                    var formated = item.companyName + ' (' + item.city + ') - ' + item.companyId
                    return formated == newCompany
                })

                if (filtered.length > 0) {
                    this.companySelected = filtered[0];
                    this.companySelectedName = this.companySelected.companyName + ' (' + this.companySelected.city + ') - ' + this.companySelected.companyId
                    this.$emit('sourceChanged', this.companySelected.companyId)
                } else {
                    this.companySelected = null
                    this.$emit('sourceChanged', '')
                }
            }
        },
        userNameStateChanged: function (username) {
            if (username == '' || username.length < 4) {
                this.isValidUsername = false
            } else {
                axios.get("/user/" + username + "/isValid").then(res => {
                    this.isValidUsername = !res.data
                }).catch(() => {
                    this.isValidUsername = false
                })
            }
        },
        resetModal() {
            this.createUser.age = null
            this.createUser.company = null
            this.createUser.firstName = ''
            this.createUser.lastName = ''
            this.createUser.userName = ''
            this.createUser.role = ''

            this.companySelected = null
            this.companySelectedName = ''

            axios.get("/company").then(response => {
                this.companyList = response.data
            })

            axios.get("/roles").then(response => {
                this.rolesList = response.data
            })
        },
        isValidUserData() {
            var valid = this.createUser.firstName.length > 2 ? true : false
            valid = valid && this.createUser.lastName.length > 0 ? true : false
            valid = valid && this.companySelected != null
            valid = valid && this.createUser.role != ''
            valid = valid && this.isValidUsername
            return valid
        },
        handleOk(model) {
            model.preventDefault()
            this.handleSubmit()
        },
        handleSubmit() {
            if (!this.isValidUserData()) {
                return
            }
            this.createUser.company = this.companySelected.companyId

            axios.post("/user", this.createUser).then(res => {
                this.successModel('Confirmation', "User Created successfully")
            }).catch(err => {
                this.failureModel("Error", err.response.data.error)
            }).finally(() => {
                this.$bvModal.hide('create-user-model')
                this.fetchUsers()
            })
        },
        successModel(t, b) {
            this.$bvModal.msgBoxOk(b, {
                title: t,
                size: 'sm',
                buttonSize: 'sm',
                okVariant: 'success',
                headerClass: 'p-2 border-bottom-0',
                footerClass: 'p-2 border-top-0',
                centered: true
            })
        },
        failureModel(t, e) {
            this.$bvModal.msgBoxOk(e, {
                title: t,
                size: 'sm',
                buttonSize: 'sm',
                okVariant: 'error',
                headerClass: 'p-2 border-bottom-0',
                footerClass: 'p-2 border-top-0',
                centered: true
            })
        }
    },
    computed: {
        rows() {
            return this.users.length
        },
        firstNameState() {
            return this.createUser.firstName.length > 2 ? true : false
        },
        lastNameState() {
            return this.createUser.lastName.length > 0 ? true : false
        },
        companyState() {
            return this.companySelected != null
        },
        rolesState() {
            var filtered = this.rolesList.filter(item => {
                return item == this.createUser.role
            })
            return filtered.length == 1
        }
    }
});

// Company List
Vue.component('company-list-view', {
    template: '#company-list-template',
    data() {
        return {
            companyFields: [
                {key: 'companyId', label: 'Company Id'},
                {key: 'companyName', label: 'Company Name'},
                {key: 'address', label: 'Address'},
                {
                    key: 'city', label: 'City',
                    formatter: (value, key, item) => {
                        return item.city + ' - ' + item.pincode
                    }
                },
                {key: 'action', label: 'Actions'}
            ],
            isBusy: true,
            companies: [],
            currentPage: 1,
            perPage: 5,
            pageOptions: [5, 10, 25, 50],
            createCompany: {
                companyName: '',
                address: '',
                city: '',
                pincode: ''
            },
            companyDetails: {
                companyId: '',
                companyName: '',
                address: '',
                city: '',
                pincode: ''
            },
        }
    },
    mounted() {
        this.getCompanyData()
    },
    computed: {
        rows() {
            return this.companies.length
        },
        isValidCompanyName() {
            return this.createCompany.companyName.length > 3
        },
        isValidCity() {
            return this.createCompany.city.length > 2
        }
    },
    methods: {
        getCompanyData() {
            this.isBusy = true
            axios.get("/company")
                .then(res => {
                    this.companies = res.data
                })
                .catch(() => {
                    this.companies = []
                })
                .finally(() => {
                    this.isBusy = false
                })
        },
        viewClicked(row) {
            this.companyDetails = row.item
            this.$bvModal.show('company-details-model')
        },
        resetCompanyDetails() {
            this.companyDetails = {
                companyId: '',
                companyName: '',
                address: '',
                city: '',
                pincode: ''
            }
        },
        deleteClicked(row) {
            axios.delete("/company/" + row.item.companyId)
                .then(res => {
                    this.getCompanyData()
                    this.successModel("Confirmation", res.data)
                })
                .catch(e => {
                    this.failureModel("Failure", e.response.data.error)
                })
        },
        resetCreateCompany() {
            this.createCompany = {
                companyName: '',
                address: '',
                city: '',
                pincode: ''
            }
        },
        handleOk(model) {
            model.preventDefault()
            this.handleSubmit()
        },
        isValidInput() {
            var isValid = this.createCompany.companyName.length > 3
            isValid = isValid & this.createCompany.city.length > 2
            return isValid
        },
        handleSubmit() {
            if (!this.isValidInput()) {
                return
            }

            axios.post("/company", this.createCompany)
                .then(res => {
                    this.successModel("Confirmation", "Company Created successfully")
                })
                .catch(error => {
                    this.failureModel("Error", error.response.data.error)
                })
                .finally(() => {
                    this.$bvModal.hide('create-company-model')
                    this.getCompanyData()
                })
        },
        successModel(t, b) {
            this.$bvModal.msgBoxOk(b, {
                title: t,
                size: 'sm',
                buttonSize: 'sm',
                okVariant: 'success',
                headerClass: 'p-2 border-bottom-0',
                footerClass: 'p-2 border-top-0',
                centered: true
            })
        },
        failureModel(t, e) {
            this.$bvModal.msgBoxOk(e, {
                title: t,
                size: 'sm',
                buttonSize: 'sm',
                okVariant: 'error',
                headerClass: 'p-2 border-bottom-0',
                footerClass: 'p-2 border-top-0',
                centered: true
            })
        }
    }
});

const user = {template: '<user-list-view></user-list-view>'}
const company = {template: '<company-list-view></company-list-view>'}

const routes = [
    {path: '', redirect: '/user'},
    {path: '/user', name: 'User', component: user},
    {path: '/company', name: 'Company', component: company}
]

const router = new VueRouter({
    routes
})

window.app = new Vue({
    el: '#app',
    router
})