drop table if exists user_role;
drop table if exists user_company;
drop table if exists role;
drop table if exists company;
drop table if exists users;

create table users
(
    user_id             int primary key not null AUTO_INCREMENT,
    username            varchar(50) unique                  not null,
    password            varchar(50)                         not null,
    first_name          varchar(50)                         not null,
    last_name           varchar(50)                         not null,
    age                 int,
    disabled            boolean   default false,
    account_expired     boolean   default false,
    account_locked      boolean   default false,
    credentials_expired boolean   default false,
    is_default_password boolean   default true,
    created_date        datetime default CURRENT_TIMESTAMP not null,
    modified_date       datetime default CURRENT_TIMESTAMP not null,
    created_by          varchar(50),
    modified_by         varchar(50)
);

create table role
(
    role_id       integer primary key not null AUTO_INCREMENT,
    role_name     varchar(50),
    created_date  datetime default CURRENT_TIMESTAMP not null,
    modified_date datetime default CURRENT_TIMESTAMP not null,
    created_by    varchar(50),
    modified_by   varchar(50)
);

create table user_role
(
    user_role_id  integer primary key not null AUTO_INCREMENT,
    user_id       integer references users (user_id),
    role_id       integer references role (role_id),
    created_date  datetime default CURRENT_TIMESTAMP not null,
    modified_date datetime default CURRENT_TIMESTAMP not null,
    created_by    varchar(50),
    modified_by   varchar(50)
);

create table company
(
    company_id    integer primary key not null AUTO_INCREMENT,
    company_name  varchar(50)                         not null,
    address       varchar(100),
    city          varchar(50)                         not null,
    pincode       varchar(10),
    created_date  datetime default CURRENT_TIMESTAMP not null,
    modified_date datetime default CURRENT_TIMESTAMP not null,
    created_by    varchar(50),
    modified_by   varchar(50)
);

create table user_company
(
    user_company_id integer primary key not null AUTO_INCREMENT,
    user_id         integer references users (user_id),
    company_id      integer references company (company_id),
    created_date    datetime default CURRENT_TIMESTAMP not null,
    modified_date   datetime default CURRENT_TIMESTAMP not null,
    created_by      varchar(50),
    modified_by     varchar(50)
);

##################################################
# Data
##################################################

# Default Roles
insert into role (role_name)
values ('ADMIN');
insert into role (role_name)
values ('USER');

# Default User
insert into users (username, password, first_name, last_name, is_default_password)
values ('admin', 'admin', 'Admin', '1', false);

# Default Role Mapping
insert into user_role (user_id, role_id)
values (1, 1);

# Default Company
insert into company (company_name, city)
values ('Built-In', 'Built-In');

# Default User Company

insert into user_company (user_id, company_id)
values (1,1);